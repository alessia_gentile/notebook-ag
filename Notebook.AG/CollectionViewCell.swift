//
//  CollectionViewCell.swift
//  Notebook.AG
//
//  Created by Alessia immacolata Gentile on 13/12/2019.
//  Copyright © 2019 Alessia immacolata Gentile. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var myImageView: UIImageView!
    
    @IBOutlet weak var myLabel: UILabel!
}
