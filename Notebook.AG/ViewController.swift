//
//  ViewController.swift
//  Notebook.AG
//
//  Created by Alessia immacolata Gentile on 13/12/2019.
//  Copyright © 2019 Alessia immacolata Gentile. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    
    
    
    @IBAction func addItem(_ sender: Any) {
        
        
    }
    @IBOutlet weak var collectionView: UICollectionView!
   
    
   
    var arrayImage = [UIImage(named:"Image1"), UIImage(named:"Image2"), UIImage(named:"Image3"),UIImage(named:"Image4"), UIImage(named: "Image5"),UIImage(named:"Image6"), UIImage(named: "Image7"), UIImage(named: "Image8")]
    
    var myLabelArray : [String] = ["Note1", "Note2", "Note3","Note4", "Note5", "Note6", "Note7", "Note8"]

    override func viewDidLoad() {
        super.viewDidLoad()
     
      
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return myLabelArray.count
    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.myImageView?.image = self.arrayImage[indexPath.row]
       // cell.myImageView.image = UIImage(named: array[indexPath.row] + ".jpg")
        cell.myLabel?.text = myLabelArray[indexPath.row]
        cell.myImageView.contentMode = .scaleAspectFill
        cell.myLabel.textColor = UIColor.magenta
        cell.myLabel.backgroundColor = UIColor.white
        cell.myLabel.contentMode = .scaleAspectFill
        //cell.myLabel.text = "Note"
        cell.myLabel.font = UIFont.boldSystemFont(ofSize: 19)
        cell.myLabel.textAlignment = .center
        cell.myLabel.highlightedTextColor = .orange
        cell.myImageView.layer.cornerRadius = 12
        cell.myImageView.layer.masksToBounds = true
     //   cell.layer.cornerRadius = 12
     //  cell..masksToBounds = true
    
return cell
}
}
